<?php

namespace App\Dto;

use Illuminate\Http\Request;

/**
 * Class CustomerSearchDto
 */
class CustomerSearchDto
{
    /**
     * @var int|null
     */
    private int|null $id = null;

    /**
     * @var string|null
     */
    private string|null $first_name = null;

    /**
     * @var string|null
     */
    private string|null $last_name = null;

    /**
     * @var string|null
     */
    private string|null $email_address = null;

    /**
     * @var string|null
     */
    private string|null $full_street_address = null;

    /**
     * @var string|null
     */
    private string|null $phone_number = null;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): string|null
    {
        return $this->first_name;
    }

    /**
     * @return string|null
     */
    public function getLastName(): string|null
    {
        return $this->last_name;
    }

    /**
     * @return string|null
     */
    public function getEmailAddress(): string|null
    {
        return $this->email_address;
    }

    /**
     * @return string|null
     */
    public function getFullStreetAddress(): string|null
    {
        return $this->full_street_address;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): string|null
    {
        return $this->phone_number;
    }

    /**
     * @param Request $request
     *
     * @return void
     */
    public function loadRequestData(Request $request): void
    {
        $this->id = $request->get('id');
        $this->first_name = $request->get('first_name');
        $this->last_name = $request->get('last_name');
        $this->email_address = $request->get('email_address');
        $this->full_street_address = $request->get('full_street_address');
        $this->phone_number = $request->get('phone_number');
    }
}
