<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{
    BelongsToMany,
    HasOne
};
use Illuminate\Support\Carbon;

/**
 * Class Customer
 *
 * @property int $id
 * @property int|null $address_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Address $address
 * @property PhoneNumber[] $phoneNumbers
 */
class Customer extends Model
{
    use HasFactory;

    /**
     * @return HasOne
     */
    public function address(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    /**
     * @return BelongsToMany
     */
    public function phoneNumbers(): BelongsToMany
    {
        return $this->belongsToMany(
            PhoneNumber::class,
            'customers_phone_numbers',
            'customer_id',
            'phone_number_id'
        );
    }
}
