<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Address
 *
 * @property int $id
 * @property string $street
 * @property string $city
 * @property string $country
 * @property string $zip
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Address extends Model
{
    use HasFactory;
}
