<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PhoneNumber
 *
 * @property int $id
 * @property string $number
 * @property int $type
 * @property string $created_at
 * @property string $updated_at
 */
class PhoneNumber extends Model
{
    use HasFactory;

    /**
     * Types
     *
     * @var int
     */
    public const TYPE_HOME = 1;
    public const TYPE_CELL = 2;
    public const TYPE_FAX = 3;
}
