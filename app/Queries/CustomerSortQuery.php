<?php

namespace App\Queries;

use App\Http\Resources\CustomerResource;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CustomerSortQuery
 */
class CustomerSortQuery
{
    /**
     * @param Builder $query
     * @param string $sortAttribute
     *
     * @return void
     */
    public function sort(Builder $query, string $sortAttribute): void
    {
        $direction = str_contains($sortAttribute, '-') ? 'desc' : 'asc';
        $sortAttribute = trim($sortAttribute, '-');
        $attribute = $this->getOriginalAttribute($sortAttribute);
        if ($attribute === null) {
            return;
        }

        $query->orderBy($attribute, $direction);
    }

    /**
     * @param string $attribute
     *
     * @return string|null
     */
    private function getOriginalAttribute(string $attribute): string|null
    {
        $map = CustomerResource::getAttributesMap();

        return $map[$attribute] ?? null;
    }
}
