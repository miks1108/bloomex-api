<?php

namespace App\Queries;

use App\Dto\CustomerSearchDto;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CustomerFilterQuery
 */
class CustomerFilterQuery
{
    /**
     * @param Builder $query
     * @param string $attribute
     * @param mixed|null $value
     *
     * @return void
     */
    public function addCondition(Builder $query, string $attribute, mixed $value = null): void
    {
        if ($value === null) {
            return;
        }
        $query->where($attribute, $value);
    }

    /**
     * @param Builder $query
     * @param string $attribute
     * @param mixed|null $value
     * @param bool $or
     *
     * @return void
     */
    public function addLikeCondition(
        Builder $query,
        string $attribute,
        mixed $value = null,
        bool $or = false
    ): void {
        if ($value === null) {
            return;
        }
        $value = mb_strtolower($value);
        $condition = <<<SQL
lower($attribute::text) LIKE '%$value%'
SQL;
        if ($or) {
            $query->orWhereRaw($condition);
        } else {
            $query->whereRaw($condition);
        }
    }

    /**
     * @param Builder $query
     * @param CustomerSearchDto $customerSearchDto
     *
     * @return void
     */
    public function filterByRelations(Builder $query, CustomerSearchDto $customerSearchDto): void
    {
        $address = $customerSearchDto->getFullStreetAddress();
        if ($address !== null) {
            $this->filterByAddressRelation($query, $address);
        }

        $phoneNumber = $customerSearchDto->getPhoneNumber();
        if ($phoneNumber !== null) {
            $this->filterByPhoneNumbersRelation($query, $phoneNumber);
        }
    }

    /**
     * @param Builder $query
     * @param string $value
     *
     * @return void
     */
    private function filterByAddressRelation(Builder $query, string $value): void
    {
        $query->join('addresses', 'customers.address_id', '=', 'addresses.id', 'left');
        $query->orWhere(function (Builder $query) use ($value): void {
            $this->addLikeCondition($query, 'addresses.street', $value, true);
            $this->addLikeCondition($query, 'addresses.city', $value, true);
            $this->addLikeCondition($query, 'addresses.country', $value, true);
            $this->addLikeCondition($query, 'addresses.zip', $value, true);
        });
    }

    /**
     * @param Builder $query
     * @param string $value
     *
     * @return void
     */
    private function filterByPhoneNumbersRelation(Builder $query, string $value): void
    {
        $query->join(
            'customers_phone_numbers',
            'customers_phone_numbers.customer_id',
            '=',
            'customers.id',
            'left'
        );
        $query->join(
            'phone_numbers',
            'phone_numbers.id',
            '=',
            'customers_phone_numbers.phone_number_id',
            'left'
        );
        $this->addLikeCondition($query, 'phone_numbers.number', $value);
    }
}
