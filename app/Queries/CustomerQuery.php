<?php

namespace App\Queries;

use App\Dto\CustomerSearchDto;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CustomerQuery
 */
class CustomerQuery
{
    /**
     * @param CustomerFilterQuery $filterQuery
     * @param CustomerSortQuery $sortQuery
     */
    public function __construct(
        private readonly CustomerFilterQuery $filterQuery,
        private readonly CustomerSortQuery $sortQuery
    ) {
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return Customer::query();
    }

    /**
     * @param CustomerSearchDto $customerSearchDto
     * @param string|null $sortAttribute
     *
     * @return Builder
     */
    public function getPreparedQuery(
        CustomerSearchDto $customerSearchDto,
        string $sortAttribute = null
    ): Builder {
        $query = $this->getQuery();

        $this->filterQuery->addCondition($query, 'id', $customerSearchDto->getId());
        $this->filterQuery->addLikeCondition(
            $query,
            'first_name',
            $customerSearchDto->getFirstName()
        );
        $this->filterQuery->addLikeCondition(
            $query,
            'last_name',
            $customerSearchDto->getLastName()
        );
        $this->filterQuery->addLikeCondition(
            $query,
            'email',
            $customerSearchDto->getEmailAddress()
        );
        $this->filterQuery->filterByRelations(
            $query,
            $customerSearchDto
        );
        if ($sortAttribute !== null) {
            $this->sortQuery->sort($query, $sortAttribute);
        }

        return $query;
    }
}
