<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CustomerGetRequest
 */
class CustomerGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => ['bail', 'integer'],
            'first_name' => ['string'],
            'last_name' => ['string'],
            'email_address' => ['string'],
            'full_street_address' => ['string'],
            'phone_number' => ['string'],
        ];
    }
}
