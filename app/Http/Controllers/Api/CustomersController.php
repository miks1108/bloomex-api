<?php

namespace App\Http\Controllers\Api;

use App\Dto\CustomerSearchDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerGetRequest;
use App\Http\Resources\CustomerResource;
use App\Queries\CustomerQuery;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class CustomersController
 */
class CustomersController extends Controller
{
    /**
     * @param CustomerSearchDto $customerSearchDto
     * @param CustomerQuery $customerQuery
     */
    public function __construct(
        private readonly CustomerSearchDto $customerSearchDto,
        private readonly CustomerQuery $customerQuery
    ) {
    }

    /**
     * @param CustomerGetRequest $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(CustomerGetRequest $request): AnonymousResourceCollection
    {
        $this->customerSearchDto->loadRequestData($request);
        $sortAttribute = $request->get('sort');
        $users = $this->customerQuery
            ->getPreparedQuery($this->customerSearchDto, $sortAttribute)
            ->with(['address', 'phoneNumbers'])
            ->paginate(20);

        return CustomerResource::collection($users);
    }
}
