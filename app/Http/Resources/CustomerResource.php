<?php

namespace App\Http\Resources;

use App\Models\{
    Address,
    PhoneNumber
};
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CustomerResource
 *
 * @property int $id
 * @property int|null $address_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property Carbon|null $created_at
 *
 * @property Address $address
 * @property PhoneNumber[] $phoneNumbers
 */
class CustomerResource extends JsonResource
{
    /**
     * @return array
     */
    public static function getAttributesMap(): array
    {
        return [
            'id' => 'id',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'email_address' => 'email'
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email_address' => $this->email,
            'full_street_address' => $this->getFullStreetAddress(),
            'phone_numbers' => PhoneNumberResource::collection($this->phoneNumbers),
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d')
        ];
    }

    /**
     * @return string
     */
    private function getFullStreetAddress(): string
    {
        if ($this->address === null) {
            return '';
        }

        return sprintf(
            '%s, %s, %s, %s',
            $this->address->street,
            $this->address->city,
            $this->address->country,
            $this->address->zip
        );
    }
}
