<?php

namespace App\Http\Resources;

use App\Models\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PhoneNumberResource
 *
 * @property string $number
 * @property int $type
 */
class PhoneNumberResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        return [
            'number' => $this->number,
            'type' => $this->getTextByType($this->type)
        ];
    }

    /**
     * @param int $type
     *
     * @return string
     */
    private function getTextByType(int $type): string
    {
        $map = $this->getTypesMap();

        return $map[$type] ?? '';
    }

    /**
     * @return array
     */
    private function getTypesMap(): array
    {
        return [
            PhoneNumber::TYPE_HOME => 'Home',
            PhoneNumber::TYPE_CELL => 'Cell',
            PhoneNumber::TYPE_FAX => 'Fax'
        ];
    }
}
