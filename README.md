
### Init

1. Install packages
```
composer install
```

2. Update Database config in .env file and make migrations
```
php artisan migrate
```

### Fill database with fake data

```
php artisan db:seed
```

### Preparing

Generate app key
```
php artisan key:generate
``` 

Set the permission on the storage folder if needed
```
sudo chmod -R 0777 storage
```

<small><i>That's all...</i></small>
