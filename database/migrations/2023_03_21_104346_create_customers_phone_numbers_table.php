<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customers_phone_numbers', function (Blueprint $table) {
            $table->primary(['customer_id', 'phone_number_id']);
            $table->foreignId('customer_id')->constrained('customers');
            $table->foreignId('phone_number_id')->constrained('phone_numbers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customers_phone_numbers');
    }
};
