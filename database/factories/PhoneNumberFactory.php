<?php

namespace Database\Factories;

use App\Models\PhoneNumber;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PhoneNumberFactory
 */
class PhoneNumberFactory extends Factory
{
    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'number' => $this->faker->e164PhoneNumber(),
            'type' => $this->getRandomType()
        ];
    }

    /**
     * @return int
     */
    private function getRandomType(): int
    {
        return $this->faker->randomElement([
            PhoneNumber::TYPE_HOME,
            PhoneNumber::TYPE_CELL,
            PhoneNumber::TYPE_FAX
        ]);
    }
}
