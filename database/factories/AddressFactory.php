<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class AddressFactory
 */
class AddressFactory extends Factory
{
    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'street' => fake()->streetName(),
            'city' => fake()->city(),
            'country' => fake()->country(),
            'zip' => fake()->postcode()
        ];
    }
}
