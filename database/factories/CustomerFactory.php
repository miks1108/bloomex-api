<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class CustomerFactory
 */
class CustomerFactory extends Factory
{
    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'address_id' => $this->getNewAddressId(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }

    /**
     * @return int
     */
    private function getNewAddressId(): int
    {
        /** @var Address $newAddress */
        $newAddress = Address::factory(1)->create()->first();

        return $newAddress->id;
    }
}
