<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\PhoneNumber;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * @var int
     */
    private const COUNT = 100;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $customers = $this->createCustomers();
        $this->createCustomerPhoneNumbersRelations($customers);
    }

    /**
     * @return Collection
     */
    private function createCustomers(): Collection
    {
        return Customer::factory(self::COUNT)->create();
    }

    /**
     * @param Collection $customers
     *
     * @return void
     */
    private function createCustomerPhoneNumbersRelations(Collection $customers): void
    {
        foreach ($customers as $customer) {
            /** @var Customer $customer */
            $phoneNumbers = $this->createPhoneNumbers();
            foreach ($phoneNumbers as $phoneNumber) {
                /** @var PhoneNumber $phoneNumber */
                $this->createCustomerPhoneNumberRelation(
                    $customer->id,
                    $phoneNumber->id
                );
            }
        }
    }

    /**
     * @param int $customerId
     * @param int $phoneNumberId
     *
     * @return void
     */
    private function createCustomerPhoneNumberRelation(int $customerId, int $phoneNumberId): void
    {
        DB::table('customers_phone_numbers')->insert([
            'customer_id' => $customerId,
            'phone_number_id' => $phoneNumberId,
        ]);
    }

    /**
     * @return Collection
     */
    private function createPhoneNumbers(): Collection
    {
        $count = rand(1, 3);

        return PhoneNumber::factory($count)->create();
    }
}
