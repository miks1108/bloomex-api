<?php

use App\Http\Controllers\Api\{
    CustomersController,
    UsersController
};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(UsersController::class)->group(function () {
    Route::get('users', 'index');
});
Route::controller(CustomersController::class)->group(function () {
    Route::get('customers', 'index');
});
